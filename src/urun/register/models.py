from django.db import models

from django.contrib.auth.models import User, AbstractUser
from django.urls import reverse


class Products(models.Model):
    name = models.CharField(max_length=30, verbose_name='Product-Name')
    location = models.CharField(max_length=40, verbose_name='City')
    date = models.DateTimeField(auto_now_add=True)
    description = models.TextField(max_length=500)
    product_image = models.ImageField(null=False, blank=True, upload_to="products_image", verbose_name='Product Image')
    STAR_SCORES = zip(range(1, 6), range(1, 6))
    product_star = models.IntegerField(choices=STAR_SCORES, null=True, blank=True, verbose_name='Star')

    class Meta:
        verbose_name = 'Urun'
        verbose_name_plural = 'Urunler'

    def __str__(self):
        return self.name + "\t" + str(self.product_star)


class Comments(models.Model):
    owner = models.ForeignKey('Products', on_delete=models.CASCADE, related_name='comments',
                              verbose_name='ChooseProduct')
    username = models.ForeignKey('UserSystem', on_delete=models.CASCADE, related_name='users', null=False,blank=False)
    comment = models.CharField(max_length=101, verbose_name='Comments')
    creation_time = models.DateTimeField(auto_now_add=True)
    STAR_SCORES = zip(range(1, 6), range(1, 6))
    star_rate = models.IntegerField(choices=STAR_SCORES, null=True, blank=True, verbose_name='Star')

    class Meta:
        verbose_name = 'Yorum'
        verbose_name_plural = 'Yorumlar'

    def __str__(self):
        return self.comment


class Company(models.Model):
    name = models.CharField(max_length=50, verbose_name='Company-Name')
    location = models.CharField(max_length=50, verbose_name='Location')
    descriptions = models.TextField(max_length=600, verbose_name='Description')
    SCORE_CHOICES = zip(range(1, 6), range(1, 6))
    stars = models.IntegerField(choices=SCORE_CHOICES, null=True, blank=True)

    class Meta:
        verbose_name = 'Sirket'
        verbose_name_plural = 'Sirketler'

    def __str__(self):
        return self.name + ' Stars: {}'.format(str(self.stars))


# Create your models here.verbose_name = 'Urun'
class UserSystem(AbstractUser):
    gsm = models.CharField(max_length=11, null=True, verbose_name='Phone Number')
    location = models.CharField(max_length=20, null=True, blank=True, verbose_name='Locaiton')
    profile_pic = models.ImageField(null=True, blank=True)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('U', 'Unlisted'),
        ('D', 'Decline To Respond')
    )
    gender = models.CharField(null=True, choices=GENDER_CHOICES, max_length=20)
    birthday = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = 'Kullanıcı'
        verbose_name_plural = 'Kullanıcılar'

