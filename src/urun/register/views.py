from django.shortcuts import render, resolve_url
from django.urls import reverse
from django.views import generic
from django.views.generic import DetailView, ListView
from django.contrib.auth.views import LoginView
from .models import UserSystem, Products, Comments
from .forms import NewUserCreation, AddProduct, User, AddComment, NewUserUpdateForm
from django.views.generic.edit import FormView, DeleteView, UpdateView


class SignUp(generic.CreateView):
    form_class = NewUserCreation
    template_name = "signup.html"
    success_url = '/login'


class LoginView(LoginView):
    model = UserSystem
    template_name = 'profile.html'


class AddProductView(generic.CreateView):
    model = Products
    form_class = AddProduct
    success_url = '/'
    template_name = 'add_product.html'


class ProfileDetailView(DetailView):
    model = UserSystem
    template_name = 'profile.html'


class ProfileUpdateView(generic.CreateView):
    model = UserSystem
    form_class = NewUserUpdateForm
    template_name = 'update_profile.html'
    success_url = '/accounts/profile'


class ProductsHomeView(ListView):
    model = Products
    template_name = 'home.html'
    context_object_name = 'product'


class ProductDetail(DetailView):
    model = Products
    template_name = 'detail_product.html'


class CommentDeleteView(DeleteView):
    model = Comments
    template_name = 'delete_comment.html'
    context_object_name = 'comments'
    success_url = '/'


class AddCommentView(generic.CreateView):
    model = Comments
    form_class = AddComment
    template_name = 'add_comment.html'
    success_url = '/'
