from django.contrib import admin
from django.http import request
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/login/<int:pk>', views.LoginView.as_view(), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('accounts/signup/', views.SignUp.as_view(), name='signup'),
    path('accounts/profile/<int:pk>', views.ProfileDetailView.as_view(), name='profile'),
    path('accounts/profile/<int:pk>/update_profile', views.ProfileUpdateView.as_view(), name='update_profile'),
    path('accounts/profile/add_product/', views.AddProductView.as_view(), name='add_product'),
    path('', views.ProductsHomeView.as_view(), name='home'),
    path('products/<int:pk>/', views.ProductDetail.as_view(), name='showproducts'),
    path('products/<int:pk>/add_comment/', views.AddCommentView.as_view(), name='add_comment'),
    path('products/<int:pk>/delete_comment/', views.CommentDeleteView.as_view(), name='delete_comment'),
]

