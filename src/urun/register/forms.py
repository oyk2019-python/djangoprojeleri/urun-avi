from django.contrib.auth.forms import *
from .models import UserSystem, Products, Comments


class NewUserCreation(UserCreationForm, forms.ModelForm):
    email = forms.EmailField(help_text='Email adresini giriniz.')

    class Meta(UserCreationForm.Meta):
        model = UserSystem
        fields = UserCreationForm.Meta.fields + ('email',)


class AddProduct(forms.ModelForm):
    class Meta:
        model = Products
        fields = ['name', 'location', 'description', 'product_image', 'product_star']


class AddComment(forms.ModelForm):
    class Meta:
        model = Comments
        fields = ['owner', 'comment', 'star_rate']


class RealDateInput(forms.DateInput):
    input_type = "date"


class NewUserUpdateForm(UserChangeForm):
    password = None

    class Meta:
        model = UserSystem
        fields = ["gsm","location","profile_pic"]
        widgets = {
            'description': forms.Textarea(),
            'username': forms.TextInput(attrs={'readonly': 'readonly'}),
            'email': forms.TextInput(attrs={'readonly': 'readonly'})
        }
