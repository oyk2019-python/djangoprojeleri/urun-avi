from django.contrib import admin
from .models import Products, Comments, Company, UserSystem
from django.contrib.auth.admin import UserAdmin


# Register your models here.

@admin.register(UserSystem)
class UserSystem(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
        ('Custom Fields', {'fields': ('gsm', 'location', 'profile_pic')}),
    )

admin.site.register(Products)
admin.site.register(Comments)
admin.site.register(Company)
